package com.example.inews;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FilteredNews extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String newsUrl;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    ArrayList<Item> itemList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filtered_news);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        SharedPreferences sharedPref = getSharedPreferences("filter", Context.MODE_PRIVATE);
        String country = sharedPref.getString("country", "");
        String sortBy = sharedPref.getString("sortBy", "");
        String sortDate = sharedPref.getString("prevDate", "");

        switch (country) {
            case "English":
                country = "en";
                break;

            case "French":
                country = "fr";
                break;
            case "Germany":
                country = "de";
                break;
            case "Italy":
                country = "it";
                break;
        }

        switch (sortBy) {
            case "Unsorted":
                sortBy = "";
                break;

            case "Popularity":
                sortBy = "popularity";
                break;
        }

        switch (sortDate) {
            case "All Date":
                sortDate = "";
                break;
            case "Previous Date":
                String pattern = "yyyy-MM-dd";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

                Calendar cal = Calendar.getInstance();

                cal.add(Calendar.DAY_OF_WEEK, -1);

                sortDate = simpleDateFormat.format(cal.getTime());
                //Toast.makeText(this, sortDate, Toast.LENGTH_SHORT).show();
                break;
        }

        //Toast.makeText(this, country, Toast.LENGTH_SHORT).show();

        newsUrl = "https://newsapi.org/v2/everything?q="+ loadData() +"&language=" + country + "&sortBy=" + sortBy + "&from=" + sortDate + "&to="+ sortDate + "&apiKey=f38f86416fa948ffa6dfbc6e8b7a11cd";
        new FilteredNews.AsyncHttpTask().execute(newsUrl);

        mRecyclerView = findViewById(R.id.recyclerView2);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        //mAdapter = new Adapter1(itemList);
        mRecyclerView.setLayoutManager(mLayoutManager);
        //mRecyclerView.setAdapter(mAdapter);
    }

    public class AsyncHttpTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                String response = streamToString(urlConnection.getInputStream());
                parseResult(response);
                return result;
            }

            catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    public String loadData() {
        SharedPreferences sharedPref = getSharedPreferences("filter", Context.MODE_PRIVATE);
        String getData = sharedPref.getString("search", "");
        //String date = sharedPref.getString("date", "");
        //Snackbar.make(findViewById(android.R.id.content), date, Snackbar.LENGTH_LONG).show();
        return getData;
    }

    String streamToString(InputStream stream) throws IOException {
        BufferedReader bufferesReader = new BufferedReader(new InputStreamReader(stream));
        String data;
        String result = "";

        while ((data = bufferesReader.readLine()) != null) {
            result += data;
        }

        if (null != stream) {
            stream.close();
        }

        return result;
    }

    void parseResult(String result) {
        JSONObject response = null;
        try {
            response = new JSONObject(result);
            JSONArray articles = response.optJSONArray("articles");

            for (int i = 0; i < articles.length(); i++) {

                JSONObject article = articles.optJSONObject(i);
                String title = article.optString("title");
                String description = article.optString("description");
                String url = article.optString("url");
                Log.i("Titles", title);
                itemList.add(new Item(R.drawable.com_facebook_tooltip_black_bottomnub, title, description, url));
            }

            mAdapter = new Adapter1(itemList);
            mRecyclerView.setAdapter(mAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.filtered_news, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, MainActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    public void openTopHeadlines() {
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);

    }

    public void openSearchNews() {
        Intent intent = new Intent(this, SearchNews.class);
        startActivity(intent);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_topHeadlines) {
            // Handle the camera action
            openTopHeadlines();
        } else if (id == R.id.nav_searchNews) {
            openSearchNews();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
