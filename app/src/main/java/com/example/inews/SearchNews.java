package com.example.inews;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SearchNews extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener {

    EditText txtSearch;
    Button btnSearch;
    String search;
    String country;
    String sortBy;
    String sortDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_news);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        txtSearch = (EditText) findViewById(R.id.txtSearch);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search = txtSearch.getText().toString();

                if (search.equals("")) {
                    Toast.makeText(SearchNews.this, "Please enter a keyword to search", Toast.LENGTH_SHORT).show();
                }

                else {

                    Intent intent = new Intent(v.getContext(), FilteredNews.class);
                    startActivity(intent);
                    startActivityForResult(intent, 0);
                    finish();
                }

                saveData();
            }
        });

        Spinner spinner = findViewById(R.id.spinner1);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.languages, R.layout.support_simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);

        Spinner spinnerSort = findViewById(R.id.spinnerSort);
        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this, R.array.sort, R.layout.support_simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerSort.setAdapter(adapter2);
        spinnerSort.setOnItemSelectedListener(this);

        Spinner spinnerDate = findViewById(R.id.spinnerDate);
        ArrayAdapter<CharSequence> adapter3 = ArrayAdapter.createFromResource(this, R.array.prevDate, R.layout.support_simple_spinner_dropdown_item);
        adapter3.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinnerDate.setAdapter(adapter3);
        spinnerDate.setOnItemSelectedListener(this);

    }

    public void saveData() {
        search = txtSearch.getText().toString();
        Date currentTime = Calendar.getInstance().getTime();
        SharedPreferences sharedPref = getSharedPreferences("filter", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("search", search);
        editor.putString("date", currentTime.toString());
        editor.putString("country", country);
        editor.putString("prevDate", sortDate);
        editor.putString("sortBy", sortBy);
        editor.apply();
        //Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
    }

    public String loadData() {
        SharedPreferences sharedPref = getSharedPreferences("filter", Context.MODE_PRIVATE);
        String getData = sharedPref.getString("search", "");
        String date = sharedPref.getString("date", "");
        Snackbar.make(findViewById(android.R.id.content), date, Snackbar.LENGTH_LONG).show();
        return getData;
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(this, "Pause", Toast.LENGTH_SHORT).show();
        saveData();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show();
        saveData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        txtSearch.setText(loadData());
        //Toast.makeText(this, "Resume", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        txtSearch.setText(loadData());
        //Toast.makeText(this, "OnStart", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_news, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //return true;

            startActivity(new Intent(this, MainActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }

    public void openTopHeadlines() {
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);

    }

    public void openSearchNews() {
        Intent intent = new Intent(this, SearchNews.class);
        startActivity(intent);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_topHeadlines) {
            // Handle the camera action
            openTopHeadlines();
        } else if (id == R.id.nav_searchNews) {
            openSearchNews();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        Spinner spinner = (Spinner)parent;


        if (spinner.getId() == R.id.spinner1)
            country = parent.getItemAtPosition(position).toString();
        else if (spinner.getId() == R.id.spinnerSort)
            sortBy = parent.getItemAtPosition(position).toString();
        else if (spinner.getId() == R.id.spinnerDate) {
            sortDate = parent.getItemAtPosition(position).toString();
        }



        //Toast.makeText(parent.getContext(), sortBy, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
