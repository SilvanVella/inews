package com.example.inews;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Pinview pinview = (Pinview)findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean b) {

                if (pinview.getValue().equals("1234")) {
                    openActivity();
                }
                else {
                    Toast.makeText(MainActivity.this, "Incorrect Pin", Toast.LENGTH_SHORT).show();

                    for (int i = 0;i < pinview.getPinLength();i++) {
                        pinview.onKey(pinview.getFocusedChild(), KeyEvent.KEYCODE_DEL, new KeyEvent(KeyEvent.ACTION_UP,KeyEvent.KEYCODE_DEL));
                    }
                }
            }
        });
    }

    public void openActivity() {
        //Intent intent = new Intent(this, Main2Activity.class);
        // startActivity(intent);

        Intent intent = new Intent(this, Login.class);
        startActivity(intent);

    }
}
